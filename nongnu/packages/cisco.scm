;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Efraim Flashner <efraim@flashner.co.il>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nongnu packages cisco)
  #:use-module (nonguix licenses)
  #:use-module (nonguix build-system binary)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages webkit)
  #:use-module (gnu packages xml))

(define-public cisco-anyconnect
  (package
    (name "cisco-anyconnect")
    (version "4.9.00086")
    (source
      (origin
        (method url-fetch)
        ;; The tarball isn't publicly available.
        (uri (string-append
               "file:///gnu/store/yh392wizp600lw4is5bav0k4i0bhlv3b-anyconnect-linux64-"
               version "-predeploy-k9.tar.gz"))
        (file-name (string-append "anyconnect-linux64-" version "-predeploy-k9.tar.gz"))
        (sha256
         (base32
          "12cgzvl2y1isgh8kjwgz2jm55y4knnw5w0iafxxdaz1dp4xlqvcf"))))
    (build-system binary-build-system)
    (arguments
     `(#:patchelf-plan
       `(("vpn/acinstallhelper"
          ("gcc:lib" "libxml2" "out"))
         ("vpn/acwebhelper"
          ("atk" "cairo" "gcc:lib" "gdk-pixbuf" "glib" "gtk+" "libc" "libsoup"
           "pango" "webkitgtk"))
         ("vpn/cfom.so"
          ("out"))
         ("vpn/libaccurl.so.4.5.0"
          ("out"))
         ("vpn/libacciscossl.so"
          ("out"))
         ("vpn/libacfeedback.so"
          ("gcc:lib" "zlib" "out"))
         ("vpn/libacruntime.so"
          ("gcc:lib" "libc" "libxml2" "zlib" "out"))
         ("vpn/libacwebhelper.so"
          ("gcc:lib" "out"))
         ("vpn/libboost_chrono.so"
          ("gcc:lib" "out"))
         ("vpn/libboost_date_time.so"
          ("gcc:lib"))
         ("vpn/libboost_filesystem.so"
          ("gcc:lib" "out"))
         ("vpn/libboost_signals.so"
          ("gcc:lib"))
         ("vpn/libboost_system.so"
          ("gcc:lib"))
         ("vpn/libboost_thread.so"
          ("gcc:lib" "out"))
         ("vpn/libvpnagentutilities.so"
          ("gcc:lib" "out"))
         ("vpn/libvpnapi.so"
          ("gcc:lib" "out"))
         ("vpn/libvpncommon.so"
          ("gcc:lib" "glib" "libc" "libxml2" "out"))
         ("vpn/libvpncommoncrypt.so"
          ("gcc:lib" "out"))
         ("vpn/libvpnipsec.so"
          ("gcc:lib" "libc" "out"))
         ("vpn/manifesttool_vpn"
          ("gcc:lib" "libc" "libxml2"))
         ("vpn/vpn"
          ("gcc:lib" "libxml2" "out"))
         ("vpn/vpnagentd"
          ("gcc:lib" "libxml2" "zlib" "out"))
         ("vpn/vpndownloader"
          ("atk" "cairo" "gdk-pixbuf" "fontconfig" "freetype" "glib" "gtk+@2"
           "gcc:lib" "libxml2" "pango" "out"))
         ("vpn/vpndownloader-cli"
          ("gcc:lib" "libxml2" "out"))
         ("vpn/vpnui"
          ("atk" "cairo" "gdk-pixbuf" "fontconfig" "freetype" "glib" "gtk+@2"
           "gcc:lib" "libxml2" "pango" "out")))
       #:install-plan
       ;; https://aur.archlinux.org/cgit/aur.git/tree/PKGBUILD?h=cisco-anyconnect
       `(("vpn/resources" "")
         ("vpn" "bin" #:include ("vpn" "vpnagentd" "vpndownloader"
                                 "vpndownloader-cli" "vpnui" "manifesttool_vpn"
                                 "acinstallhelper" "acwebhelper"))
         ("vpn" "lib" #:include-regexp ("lib.*\\.so"))
         ("vpn/cfom.so" "lib/")
         ;; Missing symlink
         ("vpn/libaccurl.so.4.5.0" "lib/libaccurl.so.4")
         ("vpn" "bin/plugins" #:include ("libacwebhelper.so" "libvpnipsec.so"))
         ("vpn/libacwebhelper.so" "bin/plugins/")
         ("vpn/libvpnipsec.so" "bin/plugins/")
         ("vpn/AnyConnectProfile.xsd" "profile/")
         ;; Are these necessary?
         ("vpn" "" #:include ("ACManifestVPN.xml" "update.txt" "AnyConnectLocalyPolicy.xsd"))
         ("vpn/resources/vpnui48.png" "share/icons/hicolor/48x48/apps/cisco-anyconnect.png")
         ("vpn/resources/vpnui64.png" "share/icons/hicolor/64x64/apps/cisco-anyconnect.png")
         ("vpn/resources/vpnui96.png" "share/icons/hicolor/96x96/apps/cisco-anyconnect.png")
         ("vpn/resources/vpnui128.png" "share/icons/hicolor/128x128/apps/cisco-anyconnect.png")
         ("vpn/resources/vpnui256.png" "share/icons/hicolor/256x256/apps/cisco-anyconnect.png")
         ("vpn/resources/vpnui512.png" "share/icons/hicolor/512x512/apps/cisco-anyconnect.png")
         ;("vpn/cisco-anyconnect.menu" "etc/xdg/menus/")
         ("vpn/cisco-anyconnect.desktop" "share/applications/")
         ("vpn/license.txt" ,,(string-append "share/doc/" name "-" version "/")))
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'patch-desktop-file
           (lambda* (#:key outputs #:allow-other-keys)
             (substitute* "vpn/cisco-anyconnect.desktop"
               (("/opt/cisco/anyconnect/bin/vpnui")
                (string-append (assoc-ref outputs "out")
                               "/bin/vpnui")))
             #t)))
       #:strip-binaries? #f))
    (inputs
     `(("atk" ,atk)
       ("cairo" ,cairo)
       ("gcc:lib" ,(canonical-package gcc) "lib")
       ("fontconfig" ,fontconfig)
       ("freetype" ,freetype)
       ("gdk-pixbuf" ,gdk-pixbuf)
       ("glib" ,glib)
       ("gtk+@2" ,gtk+-2)
       ("gtk+" ,gtk+)
       ("libsoup" ,libsoup)
       ("libxml2" ,libxml2)
       ("pango" ,pango)
       ("webkitgtk" ,webkitgtk)
       ("zlib" ,zlib)))
    (home-page "https://www.cisco.com/c/en/us/products/security/anyconnect-secure-mobility-client/index.html")
    (synopsis "Cisco AnyConnect VPN client")
    (description "This package provides the Linux version of the Cisco
AnyConnect VPN client.
There are several binaries with /opt/cisco/anyconnect hardcoded, so it is best
to install this package into a profile where it is expected.
@code{guix package -i cisco-anyconnect -p /opt/cisco/anyconnect} and to symlink
@code{ln -s /etc/ssl/certs /opt/.cisco/certificates/ca}.")
    (supported-systems '("x86_64-linux"))
    (license (nonfree "https://www.cisco.com/c/en/us/about/legal/cloud-and-software/end_user_license_agreement.html"))))
